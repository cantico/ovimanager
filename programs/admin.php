<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2006 by CANTICO ( http://www.cantico.fr )              *
************************************************************************/

include_once 'base.php';

/**
 * Renvoie le chemin du repertoire racine pour lequel PHP a acces
 */
function ovimanager_phppath()
{
    //DOCUMENT_ROOT : La racine sous laquelle le script courant est execute,
    //comme defini dans la configuration du serveur.
    //PATH_TRANSLATED : Chemin dans le systeme de fichiers (different de DOCUMENT_ROOT) jusqu'au script courant
    //On teste PATH_TRANSLATED avant DOCUMENT_ROOT
    $arboindisponible=false;
    $chemin = '';
    if (isset($_SERVER['DOCUMENT_ROOT'])) {
        //La variable DOCUMENT_ROOT n'est pas toujours accessible
        if (isset($_SERVER['DOCUMENT_ROOT'])) {
            if (substr($_SERVER['DOCUMENT_ROOT'],-1) == '/') {
                $chemin = $_SERVER['DOCUMENT_ROOT'];
            } else {
                $chemin = $_SERVER['DOCUMENT_ROOT'].'/';
            }
        } else {
            $arboindisponible=true;
        }
    } else {
        if (substr($_SERVER['PATH_TRANSLATED'],-1) == '/') {
            $chemin = $_SERVER['PATH_TRANSLATED'];
        } else {
            //Il arrive que PATH_TRANSLATED ramene le chemin avec index.php en fin
            if (substr($_SERVER['PATH_TRANSLATED'],-9) == 'index.php') {
                $chemin = substr($_SERVER['PATH_TRANSLATED'], 0, strlen($_SERVER['PATH_TRANSLATED'])-9);
            } else {
                $chemin = $_SERVER['PATH_TRANSLATED'].'/';
            }
        }
    }

    if ($arboindisponible) {
        return false;
    } else {
        return $chemin;
    }
}

/**
 * Gestion des traductions
 */
function ovimanager_translate($str)
{
    return bab_translate($str, "ovimanager");
}

/**
 * Suppression du dernier slash d'une chaine s'il existe
 */
function ovimanager_remove_last_slash($str)
{
    if (substr($str,-1) == '/') {
        return substr($str,0,-1);
    } else {
        return $str;
    }
}

/**
 * Renvoie le chemin du repertoire parent
 */
function ovimanager_folderpath($str)
{
    $chemin = str_replace("\\", "/", $str); //modifie les slashs
    if (strrpos($chemin, '/') != false) {
        $tab = explode("/", $chemin);
        if (count($tab) == 1 || count($tab) == 2) {
            return '';
        } else {
            return substr($chemin,0,strrpos($chemin, '/'));
        }
    } else {
        return '';
    }
}

/**
 * Renvoie le chemin racine d'Ovidentia
 */
function ovimanager_rootpath() {
    $script = $_SERVER['SCRIPT_FILENAME'];
    $chemin = ovimanager_folderpath($script);
    return $chemin;
}

/**
 * Renvoie le chemin du repertoire de telechargements (upload) d'Ovidentia
 */
function ovimanager_uploadpath() {
    global $babAddonFolder, $babAddonUpload, $babUploadPath;
    $chemin = str_replace("\\", "/", $babUploadPath); //modifie les slashs
    if ($chemin == '') {
        $chemin = $babAddonUpload;
    }
    return $chemin;
}

/**
 * Arborescence des dossiers et fichiers : explorateur
 */
function ovimanager_browse($path, $base = '') {
    global $babBody;

    class temp {
        var $altbg = true;

        function temp($path, $base = '') {
            $this->t_name = ovimanager_translate('Name');
            $this->t_edit = ovimanager_translate('Edit/Create a zip archive');
            $this->t_delete = ovimanager_translate('Delete');
            $this->t_zip = ovimanager_translate('Zip');
            $this->t_button = ovimanager_translate('Upload a file');
            $this->t_create = ovimanager_translate('Create a directory');
            $this->t_size = ovimanager_translate('Size');
            $this->t_time = ovimanager_translate('Date');
            $this->jsalert = ovimanager_translate('Are you sure you want to delete selected fields?');
            $this->t_perms = ovimanager_translate('Rights');
            $this->checkall = ovimanager_translate('Select all');
            $this->uncheckall = ovimanager_translate('Deselect all');
            $this->accesdirect = ovimanager_translate('Direct access');
            $this->tsite = ovimanager_translate('Racine du site');
            global $babAddonUrl;
            $csite = ovimanager_rootpath();
            //Ajoute un slash en fin
            if (substr($csite, -1, 1) != '/' && substr($csite, -1, 1) != '\'') {
                $csite = $csite.'/';
            }
            $this->csite = $babAddonUrl."admin&amp;idx=browse&amp;path=&amp;base=".urlencode($csite);
            $this->tupload = ovimanager_translate('Repertoire de telechargements');
            $cupload = ovimanager_uploadpath();
            //Ajoute un slash en fin
            if (substr($cupload, -1, 1) != '/' && substr($cupload, -1, 1) != '\'') {
                $cupload = $cupload.'/';
            }
            $this->cupload = $babAddonUrl."admin&amp;idx=browse&amp;path=&amp;base=".urlencode($cupload);
            $this->tchemin = ovimanager_translate('Chemin specifique');
            $this->tok = ovimanager_translate('OK');

            $this->dirlist = array();
            $this->dirlistview = array();
            $this->filelist = array();
            $this->base = $base;
            $this->path = $path;
            if ($this->base == '') {
                $this->base = ovimanager_rootpath(); //Affiche
                //Ajoute un slash en fin
                if (substr($this->base, -1, 1) != '/' && substr($this->base, -1, 1) != '\'') {
                    $this->base = $this->base.'/';
                }
            }
            $this->cchemin = $this->base.$this->path;

            if (is_dir($this->base.$this->path)) {
                $handle=opendir($this->base.$this->path);
                while ($file = readdir($handle))
                    {
                    if ($file == "." && $this->path != '')
                        {
                        $this->dirlist[] = '';
                        $this->dirlistview[] = '.';
                        }
                    elseif ($file == ".." && substr_count($this->path,'/') > 0)
                        {
                        $this->dirlist[] = substr($this->path,0,strrpos(ovimanager_remove_last_slash($this->path),'/')).'/';
                        $this->dirlistview[] = '..';
                        }
                    elseif ($file != "." && $file != "..")
                        {
                        if (is_dir($this->base.$this->path.$file))
                            {
                            $this->dirlist[] = $this->path.$file.'/';
                            $this->dirlistview[] = $file;
                            }
                        elseif (is_file($this->base.$this->path.$file)) {
                            $this->filelist[] = $file;
                        }
                        }
                    }
                closedir($handle);
                //Tri des fichiers et repertoires par ordre alphabetique
                for ($j=0; $j<=count($this->dirlistview)-1;$j++) {
                    for ($i=1; $i<=count($this->dirlistview)-1;$i++) {
                        if (strcasecmp($this->dirlistview[$i], $this->dirlistview[$i-1]) < 0) {
                            $save=$this->dirlist[$i-1];
                            $this->dirlist[$i-1]=$this->dirlist[$i];
                            $this->dirlist[$i]=$save;
                            $save2=$this->dirlistview[$i-1];
                            $this->dirlistview[$i-1]=$this->dirlistview[$i];
                            $this->dirlistview[$i]=$save2;
                        }
                    }
                }
                sort($this->filelist);
            } else {
                global $babBody;
                $babBody->msgerror = ovimanager_translate("The tree structure is irretrievable");
            }
        }

        function getnextdir() {
            static $i=0;
            if( $i < count($this->dirlist))
                {
                $this->displayrights = false;
                $this->altbg = !$this->altbg;
                $this->afficheiconezip = true;
                $this->name = $this->dirlistview[$i];
                $this->delete = ($this->name != '.' && $this->name != '..') ? true : false;
                $path = $this->dirlist[$i];
                if ($this->name != '.' && $this->name != '..') {
                    $this->afficheiconezip = false;
                    clearstatcache();
                    $this->perms = substr(sprintf('%o', fileperms($this->base.$path)), -4);
                    }
                else
                    $this->perms = '';

                if (function_exists('posix_getgrgid')) {
                    $usrarr = posix_getpwuid(fileowner($this->base.$path));
                    $grparr = posix_getgrgid(filegroup($this->base.$path));
                    $this->user = $usrarr['name'];
                    $this->group = $grparr['name'];
                }

                $this->tdownzip = ovimanager_translate('Create a zip archive');
                $this->checkbox = $path;
                $this->downurl = $GLOBALS['babAddonUrl']."admin&amp;idx=browse&amp;path=".urlencode($path)."&amp;base=".urlencode($this->base);
                $this->downzipurl = $GLOBALS['babAddonUrl']."admin&amp;idx=downloadzip&amp;path=".urlencode($path)."&amp;rep=".urlencode($this->name)."&amp;base=".urlencode($this->base);

                $i++;
                return true;
            } else {
                $i=0;
                return false;
            }
        }

        function getnextfile() {
            static $i=0;
            if( $i < count($this->filelist))
                {
                $this->altbg = !$this->altbg;

                $this->name = $this->filelist[$i];
                $this->size = round((filesize($this->base.$this->path.$this->name)/1024),2).' '.ovimanager_translate('Kb');
                $this->time = bab_shortDate(filemtime($this->base.$this->path.$this->name));

                $this->perms = substr(sprintf('%o', fileperms($this->base.$this->path.$this->name)), -4);

                $this->user = '';
                $this->group = '';
                $this->displayrights = false;
                if (function_exists('posix_getgrgid')) {
                    $usrarr = posix_getpwuid(fileowner($this->base.$this->path.$this->name));
                    $grparr = posix_getgrgid(filegroup($this->base.$this->path.$this->name));
                    $this->user = $usrarr['name'];
                    $this->group = $grparr['name'];
                    $this->displayrights = true;
                }

                /* The link appears only if we have rights */
                $this->tnotreading = ovimanager_translate('The file is not accessible in reading');
                $fopen = true;
                $fp = @fopen($this->base.$this->path.$this->name, 'r');
                if ($fp === false) {
                    $fopen = false;
                }
                if (is_readable($this->base.$this->path.$this->name) && $fopen) {
                    $this->downurl = $GLOBALS['babAddonUrl']."admin&amp;idx=download&amp;file=".urlencode($this->base).urlencode($this->path).urlencode($this->name);
                    $this->displaylinkdown = true;
                } else {
                    $this->displaylinkdown = false;
                    $this->downurl = '';
                }

                $this->checkbox = $this->path.$this->name;
                $this->tedit = ovimanager_translate('Edit');
                /* The link appears only if we have rights */
                if (is_readable($this->base.$this->path.$this->name) && $fopen) {
                    $this->editurl = $GLOBALS['babAddonUrl']."admin&amp;idx=edit&amp;base=".$this->base."&amp;file=".urlencode($this->path).urlencode($this->name);
                    $this->displaylinkedit = true;
                } else {
                    $this->displaylinkedit = false;
                    $this->editurl = '';
                }

                $i++;
                return true;
            } else {
                $i=0;
                return false;
            }
        }
    }
    $tp = new temp($path, $base);
    $addon = bab_getAddonInfosInstance('ovimanager');
    $babBody->babecho($addon->printTemplate($tp, "main.html", "browse"));
}

/**
 * Edition (modification) d'un fichier
 */
function ovimanager_edit($base, $file)
{
    global $babBody;

    class temp {
        function temp($base, $file) {
            $this->message = '';
            $this->pathfile = $base.$file;
            if (is_file($base.$file)) {
                $this->file = $file;
                $this->t_save = ovimanager_translate('Save');
                $this->t_close = ovimanager_translate('Close');
                $this->content = implode("", @file($base.$file));
            }
            $fopen = true;
            $fp = @fopen($base.$file, 'r+');
            if ($fp === false) {
                $fopen = false;
            }
            if (!is_writable($base.$file) || !$fopen) {
                $this->displaysavebutton = false;
                $this->message = '<h3 style="color: red">--> '.ovimanager_translate('The file is not accessible in writing').'</h3>';
            } else {
                $this->displaysavebutton = true;
            }
        }
    }
    $addon = bab_getAddonInfosInstance('ovimanager');
    $tp = new temp($base, $file);
    die($addon->printTemplate($tp, "main.html", "edit"));
}

/**
 *  Return the mime type of a file
 */
function ovimanager_mimefile($pathfile)
{
    global $babDB;
    $mime = "application/octet-stream"; /* type mime par defaut */
    if ($extension = strrchr($pathfile,".")) {
        $extension = strtolower(substr($extension,1));
        /* Types MIME */
        $typesmime = array();
        $typesmime[] = array('extension' => 'ai', 'mime' => 'application/postscript');
        $typesmime[] = array('extension' => 'asc', 'mime' => 'text/plain');
        $typesmime[] = array('extension' => 'au', 'mime' => 'audio/basic');
        $typesmime[] = array('extension' => 'avi', 'mime' => 'video/x-msvideo');
        $typesmime[] = array('extension' => 'bin', 'mime' => 'application/octet-stream');
        $typesmime[] = array('extension' => 'bmp', 'mime' => 'image/bmp');
        $typesmime[] = array('extension' => 'class', 'mime' => 'application/octet-stream');
        $typesmime[] = array('extension' => 'css', 'mime' => 'text/css');
        $typesmime[] = array('extension' => 'doc', 'mime' => 'application/msword');
        $typesmime[] = array('extension' => 'dvi', 'mime' => 'application/x-dvi');
        $typesmime[] = array('extension' => 'exe', 'mime' => 'application/octet-stream');
        $typesmime[] = array('extension' => 'flv', 'mime' => 'video/x-flv');
        $typesmime[] = array('extension' => 'gif', 'mime' => 'image/gif');
        $typesmime[] = array('extension' => 'htm', 'mime' => 'text/html');
        $typesmime[] = array('extension' => 'html', 'mime' => 'text/html');
        $typesmime[] = array('extension' => 'jpe', 'mime' => 'image/jpeg');
        $typesmime[] = array('extension' => 'jpeg', 'mime' => 'image/jpeg');
        $typesmime[] = array('extension' => 'jpg', 'mime' => 'image/jpeg');
        $typesmime[] = array('extension' => 'js', 'mime' => 'application/x-javascript');
        $typesmime[] = array('extension' => 'mid', 'mime' => 'audio/midi');
        $typesmime[] = array('extension' => 'midi', 'mime' => 'audio/midi');
        $typesmime[] = array('extension' => 'mp3', 'mime' => 'audio/mpeg');
        $typesmime[] = array('extension' => 'mpeg', 'mime' => 'video/mpeg');
        $typesmime[] = array('extension' => 'odt', 'mime' => 'application/vnd.oasis.opendocument.text');
        $typesmime[] = array('extension' => 'ods', 'mime' => 'application/vnd.oasis.opendocument.spreadsheet');
        $typesmime[] = array('extension' => 'odp', 'mime' => 'application/vnd.oasis.opendocument.presentation');
        $typesmime[] = array('extension' => 'odc', 'mime' => 'application/vnd.oasis.opendocument.chart');
        $typesmime[] = array('extension' => 'odf', 'mime' => 'application/vnd.oasis.opendocument.formula');
        $typesmime[] = array('extension' => 'odb', 'mime' => 'application/vnd.oasis.opendocument.database');
        $typesmime[] = array('extension' => 'odi', 'mime' => 'application/vnd.oasis.opendocument.image');
        $typesmime[] = array('extension' => 'odm', 'mime' => 'application/vnd.oasis.opendocument.text-master');
        $typesmime[] = array('extension' => 'ott', 'mime' => 'application/vnd.oasis.opendocument.text-template');
        $typesmime[] = array('extension' => 'ots', 'mime' => 'application/vnd.oasis.opendocument.spreadsheet-template');
        $typesmime[] = array('extension' => 'otp', 'mime' => 'application/vnd.oasis.opendocument.presentation-template');
        $typesmime[] = array('extension' => 'otg', 'mime' => 'application/vnd.oasis.opendocument.graphics-template');
        $typesmime[] = array('extension' => 'pdf', 'mime' => 'application/pdf');
        $typesmime[] = array('extension' => 'png', 'mime' => 'image/png');
        $typesmime[] = array('extension' => 'ppt', 'mime' => 'application/vnd.ms-powerpoint');
        $typesmime[] = array('extension' => 'ps', 'mime' => 'application/postscript');
        $typesmime[] = array('extension' => 'rtf', 'mime' => 'text/rtf');
        $typesmime[] = array('extension' => 'sxw', 'mime' => 'application/vnd.sun.xml.writer');
        $typesmime[] = array('extension' => 'tar', 'mime' => 'application/x-tar');
        $typesmime[] = array('extension' => 'txt', 'mime' => 'text/plain');
        $typesmime[] = array('extension' => 'wav', 'mime' => 'audio/x-wav');
        $typesmime[] = array('extension' => 'xls', 'mime' => 'application/vnd.ms-excel');
        $typesmime[] = array('extension' => 'xml', 'mime' => 'text/xml');
        $typesmime[] = array('extension' => 'zip', 'mime' => 'application/zip');
        for ($i=0;$i<=count($typesmime)-1;$i++) {
            if ($typesmime[$i]['extension'] == $extension) {
                $mime = $typesmime[$i]['mime'];
            }
        }
    }
    return $mime;
}

/**
 *  Return the name of the navigator
 */
function ovimanager_navigator()
{
    $navuser = $_SERVER['HTTP_USER_AGENT'];
    $navigateur = '';
    if (preg_match('/msie/i', $navuser) && !preg_match('/opera/i', $navuser)) {
//	if (eregi('msie', $navuser) && !eregi('opera', $navuser)) {
        //Internet Explorer
        $navigateur = 'IE';
        /* Verifie la version d'Internet Explorer
           Exemple sous IE7 : [HTTP_USER_AGENT] => Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648) */
        $tab = explode('MSIE', $_SERVER['HTTP_USER_AGENT']);
        if (isset($tab[1])) {
            $tab2 = explode(';', $tab[1]);
            if (isset($tab2[0])) {
                if (is_numeric(trim($tab2[0]))) {
                    if (trim($tab2[0]) < 6) {
                        $navigateur = 'IE5';
                    }
                    if (trim($tab2[0]) >= 6 && trim($tab2[0]) < 7) {
                        $navigateur = 'IE6';
                    }
                    if (trim($tab2[0]) >= 7 && trim($tab2[0]) < 8) {
                        $navigateur = 'IE7';
                    }
                    if (trim($tab2[0]) >= 8 && trim($tab2[0]) < 9) {
                        $navigateur = 'IE8';
                    }
                }
            }
        }
    } elseif (preg_match('/opera/i', $navuser)) {
    //} elseif (eregi('opera', $navuser)) {
        //Opera
        $navigateur = 'O';
    } elseif (preg_match('/Mozilla\/4./i', $navuser)) {
    //} elseif (eregi('Mozilla/4.', $navuser)) {
        //Netscape 4.x
        $navigateur = 'N4';
    } elseif (preg_match('/Mozilla\/5.0/i', $navuser) && !preg_match('/Konqueror/i', $navuser) && !preg_match('/Firefox/i', $navuser) && !preg_match('/Safari/i', $navuser)) {
    //} elseif (eregi('Mozilla/5.0', $navuser) && !eregi('Konqueror', $navuser) && !eregi('Firefox', $navuser) && !eregi('Safari', $navuser)) {
        //Netscape 6
        $navigateur = 'N6';
    } elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Safari')) {
        /* Safari (Mac OS) */
        $navigateur = "S";
    } elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Firefox')) {
        /* FireFox */
        $navigateur = "F";
    } elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Konqueror')) {
        /* Konqueror (Gnu/Linux KDE) */
        $navigateur="Konqueror";
    } elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Epiphany')) {
        /* Epiphany (Gnu/Linux Gnome) */
        $navigateur="Epiphany";
    } elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Lynx')) {
        /* Lynx (text browser) */
        $navigateur="Lynx";
    } else {
        //Autres navigateurs
        $navigateur = '';
    }
    return $navigateur;
}

/**
 * Telecharge un fichier
 */
function ovimanager_download($pathfile)
{
    /* Test l'existence du fichier et les droits d'acces du fichier */
    if (is_file($pathfile) && is_readable($pathfile)) {
        $nomfichier = basename($pathfile);
        $mime = ovimanager_mimefile($pathfile);
        $fsize = filesize($pathfile);
        set_time_limit(3600); //Fix the max execution time
        if(substr(ovimanager_navigator(), 0, 2) == 'IE') {// If the navigator is Internet Explorer
            header('Cache-Control: public');
        }
        header('Content-Disposition: attachment; filename="'.basename($pathfile).'"'.'\n');
        header("Content-Type: $mime"."\n");
        header("Content-Length: ". $fsize."\n");
        header("Content-transfert-encoding: binary"."\n");
        $fp = fopen($pathfile, 'rb');
        if ($fp) {
            while(!feof($fp)) {
                print fread($fp, 8192);
            }
            fclose($fp);
            exit;
        }
    }
}

/**
 * Renvoie l'arborescence accessible d'un repertoire
 */
function ovimanager_rd($d)
{
    $res = array();
    $d = substr($d,-1) != '/' ? $d.'/' : $d;
    if (is_dir($d)) {
        $handle=opendir($d);
        while ($file = readdir($handle)) {
            if ($file != "." && $file != "..") {
                if (is_dir($d.$file) && 'CVS' != $file && '.CVS' != $file ) {
                    $res = array_merge($res, ovimanager_rd($d.$file));
                } elseif (is_file($d.$file)) $res[] = $d.$file;
            }
        }
        closedir($handle);
    }
    return $res;
}

/**
 * Archive un repertoire entier en un fichier zip
 */
function ovimanager_downloadzip($base, $path,$rep)
{
    //Test si on se trouve en safe mode
    if (!get_cfg_var('safe_mode')) {
        set_time_limit(0);
    }

    //Traitement des urls
    $path = urldecode($path);
    $rep = urldecode($rep);

    $loc_in = array();
    $loc_in[0] = $base.$path;

    global $babAddonPhpPath;
    include_once $babAddonPhpPath."zip.lib.php";
    $zip = new Zip;
    $res = array();
    foreach ($loc_in as $k => $path) {
        $res = ovimanager_rd($path);
        $len = strlen($path);
        foreach ($res as $file) {
            if (is_file($file))	{
                $rec_into = substr($file,$len);
                $size = filesize($file);
                if ($size > 0) {
                    $fp = fopen($file,"r");
                    $contents = fread ($fp, $size);
                    fclose($fp);
                } else {
                    $contents = '';
                }
                $addarr[] = array($rec_into,$contents);
            }
        }
    }
    //Test si le tableau existe donc au moins un fichier dans le dossier
    if (isset($addarr)) {
        $zip->Add($addarr,0);
        header("Content-Type:application/zip");
        header("Content-Disposition: attachment; filename=".$rep.".zip");
        die($zip->get_file());
    }
}

/**
 * Enregistre le fichier
 */
function ovimanager_savefile($base, $content, $file)
{
    if (is_file($base.$file)) {
        $handle = fopen($base.$file, "w+b");
        fwrite($handle, $content);
        fclose($handle);
    }
}

/**
 * Depose un fichier
 */
function ovimanager_import($base, $path)
{
    $name = $_FILES["uploadf"]["name"];
    foreach ($GLOBALS['babFileNameTranslation'] as $char => $replace) {
        $name = str_replace($char, $replace, $name);
    }
    $downloadok = true;
    $messageerreur = ovimanager_translate('The file was deposited');
    /* Verifie les droits d'acces dans le repertoire */
    if (!is_dir($base.$path)) {
        $downloadok = false;
        $messageerreur = ovimanager_translate('The file was not deposited, the directory of destination is inaccessible');
    }
    if (!is_readable($base.$path)) {
        $downloadok = false;
        $messageerreur = ovimanager_translate('The file was not deposited, the directory of destination is inaccessible');
    }
    /* Verifie si on remplace un fichier existant et si on peut le faire */
    if (is_file($base.$path.$name)) {
        $fopen = true;
        $fp = @fopen($base.$path.$name, 'r+');
        if ($fp === false) {
            $fopen = false;
            $messageerreur = ovimanager_translate('The file was not deposited: a file of the same name exists and cannot be replaced');
        } else {
            $messageerreur = ovimanager_translate('The file was deposited and replaced the existing file');
        }
        if (!is_writable($base.$path.$name) || !$fopen) {
            $downloadok = false;
        }
    }
    if ($downloadok) {
        $res = @move_uploaded_file($_FILES["uploadf"]["tmp_name"], $base.$path.$name);
        if ($res === false) {
            $messageerreur = ovimanager_translate('The file was not deposited');
        }
    }
    global $babBody;
    $babBody->msgerror = $messageerreur;
}

/**
 * Supprime un fichier
 */
function ovimanager_deletefile($base)
{
    function deldir($dir) {
      $current_dir = opendir($dir);
      while($entryname = readdir($current_dir)){
         if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!="..")){
           deldir($dir.'/'.$entryname);
         }elseif($entryname != "." and $entryname!=".."){
           unlink($dir.'/'.$entryname);
         }
      }
      closedir($current_dir);
      rmdir($dir);
    }

    if (isset($_POST['checkedfile']) && is_array($_POST['checkedfile']) && count($_POST['checkedfile']) > 0) {
        foreach ($_POST['checkedfile'] as $file) {
            $file = $base.$file;
            if (is_dir($file)) deldir($file);
            elseif (is_file($file)) unlink($file);
        }
    }
}

/**
 * Cree un repertoire
 */
function ovimanager_create_directory($base, $path, $dir)
{
    if( !is_dir($base.$path.$dir))
        mkdir($base.$path.$dir, 0700);
}

/**
 * Formulaire pour afficher le resultat d'une requete SQL
 */
function ovimanager_sqlmanager($query)
{
    global $babBody;

    class ovimanager_sqlmanager {
        function ovimanager_sqlmanager($query) {
            global $babDB;
            $this->executebutton = ovimanager_translate('Execute the request');
            $this->sqlqueryt = $query;
            $this->message = ovimanager_translate('');
            $this->database = ovimanager_translate('Database:');
            $this->sqlresult = ovimanager_translate('Response:');
            $this->viewtables = ovimanager_translate('View tables');
            $this->viewdatas = ovimanager_translate('View datas');
            $this->basename = $GLOBALS['babDBName'];
            $this->nbresults = 0;
            $this->res = 0;
            $this->alonerequest = ovimanager_translate('Only one request is permitted');
            $this->isselectorshow = false;

            if ($query != "") {
                $db = $babDB;
                $db->errorManager(false);
                $this->compteur=0;
                $this->res = $db->db_query($query);
                //test pour savoir si la requete est de type select ou show pour utiliser mysql_num_rows
                if (strpos(strtolower($query), "select") !== false || strpos(strtolower($query), "show") !== false) {
                    $this->isselectorshow = true;
                }
                if ($this->res != 0) {
                    if ($this->isselectorshow) {
                        $this->message = ovimanager_translate('Results:')." ".$db->db_num_rows($this->res);
                    }
                    if ($this->message == ovimanager_translate('Results:')." " || $this->message == "") {
                        $this->message = ovimanager_translate('No result');
                    }
                    if ($db->db_affected_rows($this->res) != -1) {
                        $this->message .= " ; ".ovimanager_translate('Affected rows:')." ".$db->db_affected_rows($this->res);
                    } else {
                        $this->message = '<b style="color:red">'.ovimanager_translate('Mistake: invalid request')."</b>";
                    }
                    $this->nbresults = $this->res;
                } else {
                    if (function_exists('mysqli_errno') && function_exists('mysqli_error')) {
                        $this->message = '<b style="color:red">'.ovimanager_translate('Mistake: invalid request')." ".mysqli_errno()."</b><br />".mysqli_error();
                    }
                }
            } else {
                $this->message = ovimanager_translate('The request is empty');
            }
        }

        function fieldnames() {
            static $i=0;
            if ($this->isselectorshow) {
                if ($i < mysqli_num_fields($this->res)) {
                           $this->fieldname = mysqli_fetch_field_direct($this->res, $i)->name;
                    $i++;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        function fieldresults() {
            static $i=0;
            if ($this->isselectorshow) {
                if ($i < mysqli_num_rows($this->res)) {
                    $fullrow="";
                    $row_array = mysqli_fetch_row($this->res);
                    for ($j=0; $j<mysqli_num_fields($this->res); $j++) {
                              $fullrow .= '<td "style="border:1px solid black;">' . $row_array[$j] . "</td>";
                    }
                    if ($i%2) {
                        $row_color="#e5e5e5";
                    } else {
                        $row_color="#d5d5d5";
                    }
                    $this->row = '<tr bgcolor="'.$row_color."\">".$fullrow."</tr>";
                    $i++;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
    
    $addon = bab_getAddonInfosInstance('ovimanager');
    $sm = new ovimanager_sqlmanager($query);
    $babBody->babecho($addon->printTemplate($sm, "main.html", "sqlmanager"));
}

/**
 * Affiche les elements d'une base de donnees : tables et enregistrements
 */
function ovimanager_viewdatas($table, $page)
{
    global $babBody;

    class ovimanager_viewdatas {
        function ovimanager_viewdatas($table, $page) {
            global $babDB;
            $this->view = ovimanager_translate('View');
            $this->datas = ovimanager_translate('datas');
            $this->coltables = ovimanager_translate('Tables');
            $this->nbenr = ovimanager_translate('Datas');
            $this->compteur = 0;
            $this->compteur2 = 1;
            $db = $babDB;
            $this->numpage = $page;
            $this->listtables = ovimanager_translate('Back to the tables list');
            $this->nulldata = ovimanager_translate('No datas');
            $this->pagec = 1;
            $this->table = $table;
            $this->coloralter = true;

            if ($table == "") {
                //On affiche les tables de la base de donnees
                $this->tablepresent=true;
                $res = $db->db_query("show tables");
                $this->tab=array();
                $this->tab2=array();
                $this->nbres=$db->db_num_rows($res);
                if ($this->nbres != 0) {
                    $i=0;
                    while (list($name) = $db->db_fetch_array($res)) {
                        $this->tab[0][$i]=$name;
                        $res4 = $db->db_query("select * from ".$name);
                        $this->tab2[0][$i]=$db->db_num_rows($res4);
                        $i++;
                    }
                }
            } else {
                //On affiche les enregistrements de la table
                $this->tablepresent=false;
                //Savoir combien d'enregistrements en tout
                $res3 = $db->db_query("select * from ".$table);
                $this->nbres3=$db->db_num_rows($res3);
                $this->nbpages = round($this->nbres3/25);
                if (25*$this->nbpages < $this->nbres3) {
                    $this->nbpages++;
                }

                //Recuperer les 25 enregistrements de la page courante
                $thequery="select * from ".$table;
                if ($page != 1) {
                    $firstdata=($page-1)*25;
                    $thequery = $thequery." limit ".$firstdata.", 25";
                } else {
                    $thequery = $thequery." limit 0, 25";
                }
                //$thequery = $thequery." order by asc";
                $this->res2 = $db->db_query($thequery);
                $this->nbres2 = $db->db_num_rows($this->res2);
            }
        }

        function viewtable() {
            if ($this->compteur >= $this->nbres) {
                return false;
            }
            $this->name=$this->tab[0][$this->compteur];
            $this->nbdatastable=$this->tab2[0][$this->compteur];
            if ($this->coloralter) {
                $this->coloralter = false;
            } else {
                $this->coloralter = true;
            }
            $this->compteur++;
            return true;
        }

        function fieldnames() {
            static $k=0;
            if ($k < mysqli_num_fields($this->res2)) {
                       $this->fieldname = mysqli_fetch_field_direct($this->res2, $k)->name;
                $k++;
                return true;
            } else {
                return false;
            }
        }

        function fieldresults() {
            static $l=0;
            if ($l < mysqli_num_rows($this->res2)) {
                $fullrow="";
                $row_array = mysqli_fetch_row($this->res2);
                for ($j=0; $j<mysqli_num_fields($this->res2); $j++) {
                          $fullrow .= '<td "style="border:1px solid black;">' . $row_array[$j] . "</td>";
                }
                if ($l%2) {
                    $row_color="#e5e5e5";
                } else {
                    $row_color="#d5d5d5";
                }
                $this->row = '<tr bgcolor="'.$row_color."\">".$fullrow."</tr>";
                $l++;
                return true;
            } else {
                return false;
            }
        }

        function pagesmulti() {
            if ($this->compteur2 > $this->nbpages) {
                $this->compteur2=1;
                return false;
            }
            $this->pagec=$this->compteur2;
            if ($this->pagec == $this->numpage) {
                $this->currentpage = true;
            } else {
                $this->currentpage = false;
            }
            $this->compteur2++;
            return true;
        }

    }
    
    $addon = bab_getAddonInfosInstance('ovimanager');
    $vd = new ovimanager_viewdatas($table, $page);
    $babBody->babecho($addon->printTemplate($vd, "main.html", "viewdatas"));
}







function ovimanager_spoof()
{
    if (!empty($_POST)) {
        require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';
        require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
        
        $idUser = bab_getUserIdByNickname(bab_pp('nickname'));
        //bab_logout(false);
        bab_setUserSessionInfo($idUser);
        bab_url::get_request()->location();
    }
    
    $babBody = bab_getBody();
    
    $tpl = new stdClass();
    $tpl->t_nickname = ovimanager_translate('Nickname');
    $tpl->t_save = ovimanager_translate('Login');
    
    $addon = bab_getAddonInfosInstance('ovimanager');
    $babBody->babecho($addon->printTemplate($tpl, "spoof.html"));
}






// main

$idx = strval(bab_rp("idx", "browse"));
$path = strval(bab_rp("path", ''));
$base = strval(bab_rp("base", ''));
$dir = strval(bab_rp("dir", ''));
$content = strval(bab_rp("content", ''));
$file = strval(bab_rp("file", ''));
$rep = strval(bab_rp("rep", ''));
$sqlquery = strval(bab_pp("sqlquery", ''));
$sqlquery = str_replace("\n","",$sqlquery);
$sqlquery = str_replace("\r","",$sqlquery);
$table = strval(bab_gp("table", ''));
$page = intval(bab_gp("page", 1));

$action = bab_rp('action');

if (isset($action)) {
    switch ($action)
        {
        case "savefile":
            ovimanager_savefile($base, $content, $file);
            break;
        case "delete":
            ovimanager_deletefile($base);
            break;
        case "import":
            ovimanager_import($base, $path);
            break;
        case "create_directory":
            ovimanager_create_directory($base, $path, $dir);
            break;
    }
}

//Titre de la page
$title = $base.$path;
if ($title == '') {
    $title = ovimanager_rootpath(); //Affiche
    //Ajoute un slash en fin
    if (substr($title, -1, 1) != '/' && substr($title, -1, 1) != '\'') {
        $title = $title.'/';
    }
}

$babBody->setTitle(ovimanager_translate("Path:").' '.$title);

clearstatcache();

switch ($idx)
{
    case 'spoof':
        ovimanager_spoof();
        break;
        
        
    case "edit":
        ovimanager_edit($base, $file);
        break;
    case "download":
        ovimanager_download($file);
        break;
    case "downloadzip":
        ovimanager_downloadzip($base, $path, $rep);
        break;
    case "sqlmanager":
        if (isset($what)) {
            switch ($what) {
                case "viewtables" :
                    $sqlquery = "SHOW TABLES";
                    break;
            }
        }
        $sqlquery = trim($sqlquery); //suppression des espaces ou retours a la ligne en debut et fin de requete
        ovimanager_sqlmanager($sqlquery);
        $babBody->setTitle(ovimanager_translate("SQL Manager"));
        break;
    case "viewdatas":
        ovimanager_viewdatas($table, $page);
        if ($table == "") {
            $babBody->setTitle(ovimanager_translate("Tables of database ").$GLOBALS['babDBName']);
        } else {
            $babBody->setTitle(ovimanager_translate("Datas of table ").$table);
        }
        break;
    default:
    case "browse":
        ovimanager_browse($path, $base);
        break;
}

$babBody->addItemMenu("browse", ovimanager_translate("Explorer of files"),
                          $GLOBALS['babAddonUrl']."admin&idx=browse");
$babBody->addItemMenu("sqlmanager", ovimanager_translate("SQL Manager"),
                          $GLOBALS['babAddonUrl']."admin&idx=sqlmanager");
if ($idx == "viewdatas") {
    $babBody->addItemMenu("viewdatas", ovimanager_translate("Database"),
                          $GLOBALS['babAddonUrl']."admin&idx=viewdatas");
}

$babBody->addItemMenu("spoof", ovimanager_translate("Spoof user"),
    $GLOBALS['babAddonUrl']."admin&idx=spoof");

$babBody->setCurrentItemMenu($idx);
