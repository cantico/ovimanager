<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2006 by CANTICO ( http://www.cantico.fr )              *
************************************************************************/

include_once 'base.php';

function ovimanager_getAdminSectionMenus(&$url, &$text)
{
    static $nbMenus=0;
    if( !$nbMenus && !empty($GLOBALS['BAB_SESS_USERID']))
    {
        $url = $GLOBALS['babAddonUrl']."admin";
        $text = "Ovi-manager";
        $nbMenus++;
        return true;
    }

    return false;
}



function ovimanager_upgrade($version_ini, $version_base) {
    return true;
}


?>