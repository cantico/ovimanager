;<?php /*

[general]
name="ovimanager"
version="2.10.1"
encoding="UTF-8"
delete=1
description="Editor of the files on the server as well as datas in the database"
ov_version="8.1.98"
php_version="4.3.0"
author="Paul de Rosanbo; Jerome Aizier"
mysql_character_set_database="latin1,utf8"

;*/ ?>